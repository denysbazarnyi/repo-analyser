***Repo Analyser***

This repo will contain only readme and examples of public usage of Repository analyser.

**Supported VCS at the moment:**

* Git at Github.com
* Git at github.ibm.com

**Prereqisites & Limitations**

* Docker installed
* Github token is created - see details https://docs.github.com/en/rest/overview/other-authentication-methods#via-oauth-and-personal-access-tokens
* If your Github enterprise is hidden beyond corporate SSO, you will also need to authorize your personal token in the organisation - see details https://docs.github.com/en/enterprise-cloud@latest/authentication/authenticating-with-saml-single-sign-on/authorizing-a-personal-access-token-for-use-with-saml-single-sign-on
* Currently tested only on *nix OS
* Github API has a limit of 5000/h requests from one IP address per user. Keep this in mind if you will see 401 Unauthorised at some point of time. In order to verify your current available limit, you can do:
```shell
curl -u {Github-User-Name}:{github_token} -I https://{github_url}/users/{Github-User-Name}
```
Example:
```shell
curl -u Denys-Bazarnyi:{hidden_token} -I https://api.github.com/users/bazarnyi
```

**General idea of data dumping**

Currently Github API is utilised in order to download the dump of meta information from repositories:

* Commits history timestamps
* Repo name
* Branch name
* Commits depth (lines changed = lines added + lines deleted)
* All information is being pulled per developer per repo
* Start date from where you would like to pull the data is required

**Before you start**

All execution is docker based, but you will need to provide required Github credentials and requesting information to `.env` file. File template can be found in this repository. Path to `.env` file should be added to `docker run` command as follows:

`-it --env-file=.env`

*Note: `.env` file contains AUTHOR_LIST and REPO_LIST variables, where you can put list of items with whitespace delimiter, the script will parse all list items and get data for each provided commit author from each repository provided*

*Note2: `VALIDATE_CONTRIBUTORS` variable can be true or false. Github by default thinks that contributor has commits in default branch. Contributors validation will save a ton of API calls limits, but at the same time if you don't have commits in default branch yet, it will not allow script to get your commits from branches, since validation will not pass. You can always check if Github thinks you are a contributor on UI*

**Example:**
You have 2 repos:

* https://github.com/bazarnyi/app_prism
* https://github.com/bazarnyi/atomacos

In this case, your `BASE_URL` will be: `api.github.com`

`ORG_NAME` will be: `bazarnyi`

`REPO_LIST` will be: `app_prism atomacos`

`AUTHOR_LIST` should be specified as a list of either email or github username. During the testing it was discovered that some github organisations are hiding emails, thus only github usernames will work

Besides that you will need to mount a volume where you will store output of execution as `.json` and `.csv` files as follows:

`-v /path/of/host/dir:/repo_analyser/artifacts`

Image is located at public DockerHub and will be updated as long as new updates will arrive:

https://hub.docker.com/repository/docker/denbazarnyi/repo_analyser

**Examples of execution**

```shell
docker run -v /Users/db/repo/repo_analyser/artifacts:/repo_analyser/artifacts -it --env-file=.env denbazarnyi/repo_analyser
```